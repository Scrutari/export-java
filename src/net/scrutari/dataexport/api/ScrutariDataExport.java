/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface ScrutariDataExport {

    public final static int START = 1;
    public final static int BASEMETADATA = 2;
    public final static int CORPUSMETADATA = 3;
    public final static int FICHE = 4;
    public final static int THESAURUSMETADATA = 5;
    public final static int MOTCLE = 6;
    public final static int END = 7;

    public BaseMetadataExport startExport();

    public CorpusMetadataExport newCorpus(String corpusName);

    public FicheExport newFiche(String ficheId);

    public ThesaurusMetadataExport newThesaurus(String thesaurusName);

    public MotcleExport newMotcle(String motcleId);

    public void addIndexation(String corpusName, String ficheId, String thesaurusName, String motcleId, int poids);

    public void endExport();

    public int getState();

}
