/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.api;


/**
 *
 * @author Vincent Calame
 */
public interface BaseMetadataExport {

    public final static int INTITULE_SHORT = 1;
    public final static int INTITULE_LONG = 2;

    public void setAuthority(String authority);

    public void setBaseName(String baseName);

    public void setBaseIcon(String baseIcon);

    public void setIntitule(int intituleType, String lang, String intituleValue);

    public void addLangUI(String lang);

}
