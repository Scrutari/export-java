/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import net.scrutari.dataexport.api.FicheExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlFicheExport extends XmlBuilder implements FicheExport {

    private final static String[] ATTRIBUTES = {"ns", "key"};
    private final SortedMap<Integer, String> complementMap = new TreeMap<Integer, String>();
    private final Map<String, AttributeBuffer> attributeMap = new LinkedHashMap<String, AttributeBuffer>();
    private String ficheId;
    private String titre;
    private String soustitre;
    private String date;
    private String lang;
    private String href;
    private String ficheIcon;
    private String latitude;
    private String longitude;


    public XmlFicheExport() {
    }

    public void reinit(String ficheId) {
        this.ficheId = ficheId;
        this.titre = null;
        this.soustitre = null;
        this.date = null;
        this.lang = null;
        this.href = null;
        this.ficheIcon = null;
        this.latitude = null;
        this.longitude = null;
        this.complementMap.clear();
        this.attributeMap.clear();
    }

    @Override
    public void setTitre(String titre) {
        this.titre = titre;
    }

    @Override
    public void setSoustitre(String soustitre) {
        this.soustitre = soustitre;
    }

    @Override
    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public void setLang(String lang) {
        this.lang = lang;
    }

    @Override
    public void setHref(String href) {
        this.href = href;
    }

    @Override
    public void setFicheIcon(String ficheIcon) {
        this.ficheIcon = ficheIcon;
    }

    @Override
    public void setGeoloc(String latitude, String longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    @Override
    public void addComplement(int complementNumber, String complementValue) {
        if (complementNumber < 1) {
            return;
        }
        this.complementMap.put(complementNumber, complementValue);
    }

    @Override
    public void addAttributeValue(String nameSpace, String localKey, String attributeValue) {
        if (nameSpace.length() == 0) {
            return;
        }
        if (localKey.length() == 0) {
            return;
        }
        if (attributeValue.length() == 0) {
            return;
        }
        String key = nameSpace + ":" + localKey;
        AttributeBuffer buffer = attributeMap.get(key);
        if (buffer == null) {
            buffer = new AttributeBuffer(nameSpace, localKey);
            attributeMap.put(key, buffer);
        }
        buffer.addValue(attributeValue);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttribute("fiche", "fiche-id", ficheId);
        xmlWriter.addSimpleElement("titre", titre);
        xmlWriter.addSimpleElement("soustitre", soustitre);
        xmlWriter.addSimpleElement("date", date);
        xmlWriter.addSimpleElement("lang", lang);
        xmlWriter.addSimpleElement("href", href);
        xmlWriter.addSimpleElement("fiche-icon", ficheIcon);
        if ((latitude != null) && (longitude != null)) {
            xmlWriter.openTag("geoloc");
            xmlWriter.addSimpleElement("lat", latitude);
            xmlWriter.addSimpleElement("lon", longitude);
            xmlWriter.closeTag("geoloc");
        }
        if (complementMap.size() > 0) {
            int max = complementMap.lastKey();
            for (int i = 1; i <= max; i++) {
                String s = complementMap.get(new Integer(i));
                if ((s == null) || (s.length() == 0)) {
                    xmlWriter.addEmptyElement("complement", null, null);
                } else {
                    xmlWriter.addSimpleElement("complement", s);
                }
            }
        }
        for (AttributeBuffer buffer : attributeMap.values()) {
            buffer.writeXML(xmlWriter);
        }
        xmlWriter.closeTag("fiche");
    }


    private static class AttributeBuffer {

        private final String[] array = new String[2];
        private final List<String> valueList = new ArrayList<String>();

        private AttributeBuffer(String nameSpace, String localKey) {
            this.array[0] = nameSpace;
            this.array[1] = localKey;
        }

        private void addValue(String value) {
            valueList.add(value);
        }

        private void writeXML(XmlWriter xmlWriter) {
            xmlWriter.openTagWithAttributes("attr", ATTRIBUTES, array);
            for (String value : valueList) {
                xmlWriter.addSimpleElement("val", value);
            }
            xmlWriter.closeTag("attr");
        }

    }

}
