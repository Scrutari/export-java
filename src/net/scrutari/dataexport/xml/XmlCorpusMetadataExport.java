/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import net.scrutari.dataexport.api.CorpusMetadataExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlCorpusMetadataExport extends XmlBuilder implements CorpusMetadataExport {


    private final Map<String, String> corpusMap = new LinkedHashMap<String, String>();
    private final Map<String, String> ficheMap = new LinkedHashMap<String, String>();
    private final List<Map<String, String>> complementMapList = new ArrayList<Map<String, String>>();
    private String corpusIcon;
    private String hrefParent;

    public XmlCorpusMetadataExport() {

    }

    @Override
    public void setCorpusIcon(String corpusIcon) {
        this.corpusIcon = corpusIcon;
    }

    @Override
    public void setHrefParent(String hrefParent) {
        this.hrefParent = hrefParent;
    }

    @Override
    public void setIntitule(int intituleType, String lang, String intituleValue) {
        Map<String, String> map = getMap(intituleType);
        map.put(lang, intituleValue);
    }

    @Override
    public int addComplement() {
        complementMapList.add(new LinkedHashMap<String, String>());
        return complementMapList.size();
    }

    @Override
    public void setComplementIntitule(int complementNumber, String lang, String intituleValue) {
        if ((complementNumber < 0) || (complementNumber > complementMapList.size())) {
            return;
        }
        Map<String, String> map = complementMapList.get(complementNumber - 1);
        map.put(lang, intituleValue);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTag("corpus-metadata");
        addMap(INTITULE_CORPUS, xmlWriter);
        addMap(INTITULE_FICHE, xmlWriter);
        xmlWriter.addSimpleElement("href-parent", hrefParent);
        xmlWriter.addSimpleElement("corpus-icon", corpusIcon);
        for (Map<String, String> map : complementMapList) {
            addComplementMap(map, xmlWriter);
        }
        xmlWriter.closeTag("corpus-metadata");
    }

    private void addComplementMap(Map<String, String> map, XmlWriter xmlWriter) {
        xmlWriter.openTag("complement-metadata");
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("complement-metadata");
    }

    private void addMap(int intituleType, XmlWriter xmlWriter) {
        Map<String, String> map = getMap(intituleType);
        if (map.isEmpty()) {
            return;
        }
        String suffix = getSuffix(intituleType);
        xmlWriter.openTag("intitule-" + suffix);
        for (Map.Entry<String, String> entry : map.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("intitule-" + suffix);
    }

    private Map<String, String> getMap(int type) {
        switch (type) {
            case INTITULE_CORPUS:
                return corpusMap;
            case INTITULE_FICHE:
                return ficheMap;
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

    private static String getSuffix(int type) {
        switch (type) {
            case INTITULE_CORPUS:
                return "corpus";
            case INTITULE_FICHE:
                return "fiche";
            default:
                throw new IllegalArgumentException("Wrong intituleType = " + type);
        }
    }

}
