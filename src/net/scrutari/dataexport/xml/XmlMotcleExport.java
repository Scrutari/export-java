/* ScrutariDataExport - Java implementation of ScrutariDataExport API
 * http://www.scrutari.net/dokuwiki/scrutaridata:exportapi
 *
 * Copyright (c) 2010-2016 Vincent Calame - Exemole
 * Licensed under MIT
 * (http://en.wikipedia.org/wiki/MIT_License)
 */


package net.scrutari.dataexport.xml;

import java.util.LinkedHashMap;
import java.util.Map;
import net.scrutari.dataexport.api.MotcleExport;


/**
 *
 * @author Vincent Calame
 */
public class XmlMotcleExport extends XmlBuilder implements MotcleExport {


    private final Map<String, String> libelleMap = new LinkedHashMap<String, String>();
    private String motcleId;

    public XmlMotcleExport() {

    }

    public void reinit(String motcleId) {
        this.motcleId = motcleId;
        this.libelleMap.clear();
    }

    @Override
    public void setLibelle(String lang, String libelleValue) {
        libelleMap.put(lang, libelleValue);
    }

    @Override
    public void writeXML(XmlWriter xmlWriter) {
        xmlWriter.openTagWithAttribute("motcle", "motcle-id", motcleId);
        for (Map.Entry<String, String> entry : libelleMap.entrySet()) {
            xmlWriter.addLibElement(entry.getKey(), entry.getValue());
        }
        xmlWriter.closeTag("motcle");
    }

}
